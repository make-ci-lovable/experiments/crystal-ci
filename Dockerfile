# Crystal for GitLab CI 2020-10-29 by @k33g | on gitlab.com 
FROM crystallang/crystal:latest-alpine

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

# 🚧 wip
RUN apk --update add --no-cache curl

COPY crystal_run.sh /usr/local/bin/crystal_run
RUN chmod +x /usr/local/bin/crystal_run

CMD ["/bin/sh"]

