#!/bin/sh
# https://stackoverflow.com/questions/4380478/indirect-parameter-substitution-in-shell-script
param=${1}
eval source_code=\"\$$param\"
echo "$source_code" > ${1}.cr
crystal ${1}.cr
